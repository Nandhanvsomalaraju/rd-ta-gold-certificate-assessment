package com.miniproject.constants;

public class UI_Constants {
	
	public static final long WAIT_TIME = 7;
	public static String BROWSER_PROPERTY = "browser";
	public static String BROWSER_PROPERTY_FILE_PATH = "C:\\Users\\Nandhanvarma_Somalar\\Desktop\\TA-MiniProject\\miniproject\\src\\test\\resources\\browser.properties";
	public static String LAUNCH_URL = "https://demoqa.com/login";
	
	public static String BOOKS_URL = "https://demoqa.com/books";
}
