package com.miniproject.constants;

public class API_Constants {
	
	public static final String CREATE_USER_BASE_URI = "https://demoqa.com/";
	public static final String CREATE_USER_PATH = "Account/v1/User";
	public static final String MESSAGE_FOR_MISSING_FIELDS = "UserName and Password required.";
	public static final String MESSAGE_FOR_INVALID_PASSWORD_FORMAT = "Passwords must have at least one non alphanumeric character, one digit ('0'-'9'), one uppercase ('A'-'Z'), one lowercase ('a'-'z'), one special character and Password must be eight characters or longer.";
	public static final String CODE_FOR_INCORRECT_PASSWORD = "1300";
	public static final String CODE_FOR_INVALID_FORMAT = "1200";
	public static final String GET_BOOKS_BASE_URI = "https://demoqa.com/";
	public static final String GET_BOOKS_PATH = "BookStore/v1/Books";
	public static final String MESSAGE = "message";
	public static final String BODY_CODE = "code";
	public static String USERNAME = "Adam12345";
}
