package com.miniproject.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(glue= {"com.miniproject.ui.stepdefinitions"},features= {"C:\\Users\\Nandhanvarma_Somalar\\Desktop\\TA-MiniProject\\miniproject\\src\\test\\resources\\features\\uifeatures"}
,monochrome = true,plugin={"html:target/cucumber.html",
"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},publish = true)
public class UIRunner extends AbstractTestNGCucumberTests{
	

}
