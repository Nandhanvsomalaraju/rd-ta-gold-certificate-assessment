package com.miniproject.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(glue= {"com.miniproject.api.StepDefinitions"},features= {"C:\\Users\\Nandhanvarma_Somalar\\Desktop\\TA-MiniProject\\miniproject\\src\\test\\resources\\features\\apifeatures"},
plugin={"html:target/cucumber.html",
"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"})
public class APIRunner extends AbstractTestNGCucumberTests{

}
