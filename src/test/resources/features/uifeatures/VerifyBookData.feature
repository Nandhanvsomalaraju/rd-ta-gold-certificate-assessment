Feature: Verify Whether The Books Extracted From API Are Present Or Not


Scenario Outline: Check Whether The Books Present Or Not
Given User Is In The BooksPage
When User Searches With A Partial Text Of Book Name "<Book>"
Then Verify Whether Book Is Present Or Not
Examples:
|Book|
|Git|