Feature: Verify Whether The User Has Created


Scenario Outline: Checking Whether The Username Displayed Is Same Or Not
	Given User Launches The LoginPage
	When User enters "<username>" and "<password>"
	And User Clicks on login button
	Then Verify Whether username is displayed correct or not
	Examples:
	|username| |password|
	|Adam123| |Hello@8505| 