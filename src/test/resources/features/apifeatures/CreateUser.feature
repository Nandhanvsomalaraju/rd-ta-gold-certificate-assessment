Feature: Checking The Create User Functionality

Scenario Outline: User Hits The EndPoint URI With Correct Details
	Given User hits the base uri
	When User makes a post request with required details "<username>" and "<password>"
	Then Validate the Status Code
	And Validate the response body
	Examples:
	|username| |password|
	|Adam12345| |Hello@8505|
@Negative
Scenario: User Hits The EndPoint URI With Empty Credentials
	Given User hits the base uri
	When User makes a post request with empty credentials
	Then Validate the status code of request after missing credentials
	And Validate the response body of request after missing credentials
@Negative
Scenario Outline: User Hits The EndPoint URI With Missing Username
 Given User hits the base uri
 When User makes a post request without username "<password>"
 Then Validate the status code after missing username
 And Validate the response body after missing username
 Examples:
 |password|
 |Hello@8505|
@Negative
Scenario Outline: User Hits The EndPoint URI With Missing Password
	Given User hits the base uri
	When User makes a post request without password "<username>"
	Then Validate the status code after missing the password
	And Validate the response body after missing the password
	Examples:
	|username|
	|Hello1234|
@Negative
Scenario Outline: Users Hits The EndPoint URI With Password Less Than Required Characters
	Given User hits the base uri
	When User makes a post request with the password having less characters "<username>" and "<password>"
	Then Validate the status code after entering password with less characters
	And Validate the response body after entering password with less characters
	Examples:
	|username| |password|
	|John| |World|
	

		