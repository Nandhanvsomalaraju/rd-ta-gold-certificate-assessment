package com.miniproject.ui.stepdefinitions;

import org.testng.Assert;

import com.miniproject.api.StepDefinitions.BookInfoStepDefinition;
import com.miniproject.ui.pages.BookPage;
import com.miniproject.ui.utils.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class VerifyBookPresentStepDefinition {
	
	public TestContext testContext;
	public BookPage bookPage;
	static JsonPath jsonPathOfObtainedBooks = BookInfoStepDefinition.jsonPathOfBooks;
	
	VerifyBookPresentStepDefinition(TestContext testContext)
	{
		this.testContext = testContext;
		this.bookPage = this.testContext.pageObjectManager.getBookPage();
	}
	
	@Given("User Is In The BooksPage")
	public void user_is_in_the_books_page() {
		bookPage.gotoBookPage();

	}
	@When("User Searches With A Partial Text Of Book Name {string}")
	public void user_searches_with_a_partial_text_of_book_name(String bookName) {
		bookPage.searchForBook(bookName);
		

	}
	@Then("Verify Whether Book Is Present Or Not")
	public void verify_whether_book_is_present_or_not() {
		String bookAuthorName = bookPage.getAuthorName();
		String bookPublisherName = bookPage.getpublisherName();
		
		Assert.assertEquals(bookAuthorName,jsonPathOfObtainedBooks.getString("books[0].author") );
		Assert.assertEquals(bookAuthorName,jsonPathOfObtainedBooks.getString("publisher") );
	}

}
