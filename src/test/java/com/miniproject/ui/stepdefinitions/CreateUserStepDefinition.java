package com.miniproject.ui.stepdefinitions;

import com.miniproject.ui.pages.LoginPage;
import com.miniproject.ui.pages.ProfilePage;
import com.miniproject.ui.utils.TestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CreateUserStepDefinition {
	
	public TestContext testContext;
	public LoginPage loginPage;
	public ProfilePage profilePage;
	
	public CreateUserStepDefinition(TestContext testContext)
	{
		this.testContext = testContext;
		this.loginPage = this.testContext.pageObjectManager.getLoginPage();
		this.profilePage = this.testContext.pageObjectManager.getProfilePage();
	}
	
	
	@Given("User Launches The LoginPage")
	public void user_launches_the_login_page() {
		loginPage.launchURL();

	}

	@When("User enters {string} and {string}")
	public void user_enters_and(String username, String password) {
		loginPage.enterUsername(username);
		loginPage.enterPassword(password);
	}

	@When("User Clicks on login button")
	public void user_clicks_on_login_button() {
		profilePage = loginPage.clickLoginButton();

	}

	@Then("Verify Whether username is displayed correct or not")
	public void verify_whether_username_is_displayed_correct_or_not() {
		System.out.println(profilePage.getUserName());
	}

}
