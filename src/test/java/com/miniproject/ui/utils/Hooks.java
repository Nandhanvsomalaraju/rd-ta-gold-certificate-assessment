package com.miniproject.ui.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Scenario;

public class Hooks {
	
	public TestContext testContext;
	
	public Hooks(TestContext testContext)
	{
		this.testContext = testContext;
	}
	
	@After()
	public void quitBrowser() throws IOException
	{
		this.testContext.driverManager.getDriver().quit();
	}
	
	@AfterStep
	public void takeScreenShot(Scenario scenario) throws IOException
	{
        WebDriver driver = testContext.driverManager.getDriver();
        if(scenario.isFailed())
        {
            File sourcePath = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            byte[] fileContent = FileUtils.readFileToByteArray(sourcePath);
            scenario.attach(fileContent,"image/png","image");

        }
	}


}
