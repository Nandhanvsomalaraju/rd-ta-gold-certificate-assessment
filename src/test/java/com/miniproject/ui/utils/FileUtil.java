package com.miniproject.ui.utils;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;


public class FileUtil {

	public static String readPropertyFromPropertyFile(String path, String propertyName) throws IOException
	{
		FileInputStream fileInputStream = new FileInputStream(path);
		
		Properties properties = new Properties();
		
		properties.load(fileInputStream);
		
		return properties.getProperty(propertyName);

	}
}
