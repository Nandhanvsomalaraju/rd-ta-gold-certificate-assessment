package com.miniproject.ui.utils;

import java.io.IOException;

import com.miniproject.ui.pages.PageObjectManager;



public class TestContext {
	
	public DriverManager driverManager;
	public PageObjectManager pageObjectManager;
	
	public TestContext()
	{
		this.driverManager = new DriverManager();
		this.pageObjectManager = new PageObjectManager(driverManager.getDriver());
	}
	

}
