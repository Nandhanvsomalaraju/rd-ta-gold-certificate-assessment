package com.miniproject.ui.utils;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import com.miniproject.constants.UI_Constants;
import com.miniproject.ui.browserdrivers.BrowserFactory;

public class DriverManager {
	
	public  String browserName;
	public static WebDriver driver;
	
	public  String getBrowserName()
	{
		try {
			
			browserName = FileUtil.readPropertyFromPropertyFile(UI_Constants.BROWSER_PROPERTY_FILE_PATH, UI_Constants.BROWSER_PROPERTY);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return browserName;
	}
	
	
	
	public WebDriver getDriver()
	{
		if(driver == null)
		{
			driver = BrowserFactory.getRequiredBrowserDriver(getBrowserName());
		}
		return driver;
	}
	

}
