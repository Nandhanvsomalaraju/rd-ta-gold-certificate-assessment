package com.miniproject.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.miniproject.constants.UI_Constants;

public class BookPage {
	
	public WebDriver driver;
	
	public BookPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//input[@id='searchBox']")
	private WebElement searchBox;
	
	@FindBy(xpath="(//div[@class='rt-td'])[3]")
	private WebElement authorName;
	
	@FindBy(xpath="(//div[@class='rt-td'])[4]")
	private WebElement publisherName;
	
	public void gotoBookPage()
	{
		driver.get(UI_Constants.BOOKS_URL);
	}
	
	public void searchForBook(String bookName)
	{
		searchBox.sendKeys(bookName);
	}
	
	public String getAuthorName()
	{
		return authorName.getText();
	}
	
	public String getpublisherName()
	{
		return publisherName.getText();
	}
	
	
	
}
