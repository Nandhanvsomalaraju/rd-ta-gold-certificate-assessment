package com.miniproject.ui.pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.miniproject.constants.UI_Constants;

public class ProfilePage {
	
	public WebDriver driver;
	
	public ProfilePage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[@class='profile-wrapper']//label[@id='userName-value']")
	private WebElement userName;
	
	@FindBy(xpath="(//button[@id='submit'])[1]")
	private WebElement logoutButton;
	
	By usernameElement = By.cssSelector("userName-value");
	
	public String getUserName()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", userName);
		return userName.getText();
	}
	
	public LoginPage clickLogOutButton()
	{
		logoutButton.click();
		return new LoginPage(driver);
	}
	
	
	
	

}
