package com.miniproject.ui.pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.miniproject.constants.UI_Constants;

public class LoginPage {
	
	public WebDriver driver;
	
	public LoginPage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="userName")
	private WebElement userNameInputField;
	
	@FindBy(id="password")
	private WebElement passwordInputField;
	
	@FindBy(id="login")
	private WebElement loginButton;
	
	By loginButtonElement = By.id("login");
	
	public void enterUsername(String name)
	{
		userNameInputField.sendKeys(name);
	}
	
	public void enterPassword(String password)
	{
		passwordInputField.sendKeys(password);
	}
	
    
	public ProfilePage clickLoginButton()
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(UI_Constants.WAIT_TIME));
		wait.until(ExpectedConditions.elementToBeClickable(loginButtonElement));
		
		js.executeScript("arguments[0].scrollIntoView();", loginButton);
		loginButton.click();
		return new ProfilePage(driver);
	}
	
	public void launchURL()
	{
		driver.get(UI_Constants.LAUNCH_URL);
	}
	
	

}
