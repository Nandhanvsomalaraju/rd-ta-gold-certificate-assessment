package com.miniproject.ui.pages;

import org.openqa.selenium.WebDriver;

public class PageObjectManager {
	
	public WebDriver driver;
	public LoginPage loginPage;
	public ProfilePage profilePage;
	public BookPage bookPage;
	
	public PageObjectManager(WebDriver driver)
	{
		this.driver = driver;
	}
	public LoginPage getLoginPage()
	{
		if(loginPage == null)
		{
			loginPage =  new LoginPage(driver);
		}
		return loginPage;
		
	}
	
	public ProfilePage getProfilePage()
	{
		if(profilePage == null)
		{
			profilePage =  new ProfilePage(driver);
		}
		return profilePage;
		
	}
	
	public BookPage getBookPage()
	{
		if(bookPage == null)
		{
			bookPage =  new BookPage(driver);
		}
		return bookPage;
		
	}
}
