package com.miniproject.ui.browserdrivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxBrowserDriver {
	
	public WebDriver driver;
	
	public WebDriver getFirefoxBrowserDriver()
	{

		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		return driver;
	}

}
