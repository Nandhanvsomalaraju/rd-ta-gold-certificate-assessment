package com.miniproject.ui.browserdrivers;

import org.openqa.selenium.WebDriver;

public class BrowserFactory {
	
	public static WebDriver driver;
	
	public static WebDriver getRequiredBrowserDriver(String name)
	{
		if(name.equalsIgnoreCase("chrome"))
		{
			driver = new ChromeBrowserDriver().getChromeBrowserDriver();
		}
		else if(name.equalsIgnoreCase("edge"))
		{
			driver = new EdgeBrowserDriver().getEdgeBrowserDriver();
		}
		else if(name.equalsIgnoreCase("firefox"))
		{
			driver = new FirefoxBrowserDriver().getFirefoxBrowserDriver();
		}
		return driver;
		
	}

}
