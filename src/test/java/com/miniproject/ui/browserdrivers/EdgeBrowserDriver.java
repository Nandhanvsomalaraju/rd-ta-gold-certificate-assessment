package com.miniproject.ui.browserdrivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class EdgeBrowserDriver {
	
	public WebDriver driver;
	
	public WebDriver getEdgeBrowserDriver()
	{

		driver = new EdgeDriver();
		driver.manage().window().maximize();
		return driver;
	}

}
