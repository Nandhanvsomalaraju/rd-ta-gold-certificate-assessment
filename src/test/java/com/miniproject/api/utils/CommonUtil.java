package com.miniproject.api.utils;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

//import static io.restassured.RestAssured.*;
//import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import com.miniproject.constants.API_Constants;
public class CommonUtil {

	
	public static RequestSpecification requestSpec; 
	public static RequestSpecification requestSpecForBook; 
	public RequestSpecification createRequest() throws IOException{
		if(requestSpec == null)
		{
			PrintStream log = new PrintStream(new File("createuserlog.txt"));
			new RequestLoggingFilter();
			requestSpec = new RequestSpecBuilder().setBaseUri(API_Constants.CREATE_USER_BASE_URI)
					.addFilter(RequestLoggingFilter.logRequestTo(log)).addFilter(ResponseLoggingFilter.logResponseTo(log)).build();
			}
		return requestSpec;
	}
	
	public RequestSpecification requestforBook() throws IOException{
		if(requestSpecForBook == null)
		{
			PrintStream log = new PrintStream(new File("getBooks.txt"));
			new RequestLoggingFilter();
			requestSpecForBook = new RequestSpecBuilder().setBaseUri(API_Constants.GET_BOOKS_BASE_URI)
					.addFilter(RequestLoggingFilter.logRequestTo(log)).addFilter(ResponseLoggingFilter.logResponseTo(log)).build();
			}
		return requestSpec;
	}
	
	public ResponseSpecification validateCodeForCorrectDetails()
	{
		return new ResponseSpecBuilder().expectStatusCode(201).build();
	}
	
	public ResponseSpecification validateCodeForIncorrectDetails()
	{
		return new ResponseSpecBuilder().expectStatusCode(400).build();
	}
	
}
