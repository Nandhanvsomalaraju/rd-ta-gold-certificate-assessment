package com.miniproject.api.testdata;

import com.miniproject.api.utils.CommonUtil;

public class APITestContextSetup {
	
	public CommonUtil utils;
	public CreateUserData userData;
	
	public APITestContextSetup()
	{
		this.utils = new CommonUtil();
		this.userData = new CreateUserData();
	}

}
