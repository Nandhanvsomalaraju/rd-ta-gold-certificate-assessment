package com.miniproject.api.testdata;

import com.miniproject.api.pojos.UserPOJO;

public class CreateUserData {
	
	public UserPOJO createUserWithCorrectDetails(String username,String password)
	{
		UserPOJO validUser = new UserPOJO();
		
		validUser.setUserName(username);
		validUser.setPassword(password);
		
		return validUser;
	}
	
	public UserPOJO createUserWithMissingPassword(String username)
	{
		UserPOJO userMissingPassword = new UserPOJO();
		
		userMissingPassword.setUserName(username);
		
		return userMissingPassword;
	}
	
	
	public  UserPOJO createUserWithMissingUsername(String password)
	{
		UserPOJO userMissingUsername = new UserPOJO();

		userMissingUsername.setPassword(password);
		
		return userMissingUsername;
	}
	
	public UserPOJO createUserWithEmptyCredentials()
	{
		UserPOJO userWithNoCredentials = new UserPOJO();
		return userWithNoCredentials;
	}
	
	public UserPOJO createUserWithPasswordLessThanRequiredCharacters(String username,String password)
	{
		UserPOJO userWithPasswordLessThanRequiredCharacters = new UserPOJO();
		
		userWithPasswordLessThanRequiredCharacters.setUserName(username);
		userWithPasswordLessThanRequiredCharacters.setPassword(password);
		
		return userWithPasswordLessThanRequiredCharacters;
	}
	

}
