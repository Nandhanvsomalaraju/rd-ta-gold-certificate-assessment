package com.miniproject.api.StepDefinitions;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.*;


import java.io.IOException;

import org.testng.Assert;

import com.miniproject.api.testdata.APITestContextSetup;
import com.miniproject.constants.API_Constants;

public class CreateUserStepDefinition {
	
	RequestSpecification requestSpecificationForUser;
	Response response;
	APITestContextSetup testContext;
	public CreateUserStepDefinition(APITestContextSetup testContext)
	{
		this.testContext = testContext;
	}
	@Given("User hits the base uri")
	public void user_hits_the_base_uri() throws IOException {
		
		requestSpecificationForUser = given().spec(testContext.utils.createRequest());

	}

	@When("User makes a post request with required details {string} and {string}")
	public void user_makes_a_post_request_with_required_details_and(String username, String password) {
		response = requestSpecificationForUser.contentType(ContentType.JSON).
		body(testContext.userData.createUserWithCorrectDetails(username, password)).when().post(API_Constants.CREATE_USER_PATH);

	}

	@Then("Validate the Status Code")
	public void validate_the_status_code() {
	response.then().spec(testContext.utils.validateCodeForCorrectDetails());

	}

	@Then("Validate the response body")
	public void validate_the_response_body() {
		JsonPath jsonPath = response.then().extract().jsonPath();
		String userName = jsonPath.getString("username");
		Assert.assertEquals(userName, API_Constants.USERNAME);
	}

	@When("User makes a post request with empty credentials")
	public void user_makes_a_post_request_with_empty_credentials() {
		
		response = requestSpecificationForUser.contentType(ContentType.JSON).
				body(testContext.userData.createUserWithEmptyCredentials()).when().post(API_Constants.CREATE_USER_PATH);

	}

	@Then("Validate the status code of request after missing credentials")
	public void validate_the_status_code_of_request_after_missing_credentials() {
		
		response.then().spec(testContext.utils.validateCodeForIncorrectDetails());

	}

	@Then("Validate the response body of request after missing credentials")
	public void validate_the_response_body_of_request_after_missing_credentials() {
		JsonPath jsonPath = response.then().extract().jsonPath();
		String message = jsonPath.getString("message");
		String code = jsonPath.getString("code");
		Assert.assertEquals(code, API_Constants.CODE_FOR_INVALID_FORMAT);
		Assert.assertEquals(message, API_Constants.MESSAGE_FOR_MISSING_FIELDS);
	}

	@When("User makes a post request without username {string}")
	public void user_makes_a_post_request_without_username(String password) {
		response = requestSpecificationForUser.contentType(ContentType.JSON).
				body(testContext.userData.createUserWithMissingUsername(password)).when().post(API_Constants.CREATE_USER_PATH);
	}

	@Then("Validate the status code after missing username")
	public void validate_the_status_code_after_missing_username() {
		response.then().spec(testContext.utils.validateCodeForIncorrectDetails());
	}

	@Then("Validate the response body after missing username")
	public void validate_the_response_body_after_missing_username() {
		JsonPath jsonPath = response.then().extract().jsonPath();
		String message = jsonPath.getString(API_Constants.MESSAGE);
		String code = jsonPath.getString(API_Constants.BODY_CODE);
		Assert.assertEquals(code, API_Constants.CODE_FOR_INVALID_FORMAT);
		Assert.assertEquals(message, API_Constants.MESSAGE_FOR_MISSING_FIELDS);
	}

	@When("User makes a post request without password {string}")
	public void user_makes_a_post_request_without_password(String username) {
		
		response = requestSpecificationForUser.contentType(ContentType.JSON).
				body(testContext.userData.createUserWithMissingPassword(username)).when().post(API_Constants.CREATE_USER_PATH);
	}

	@Then("Validate the status code after missing the password")
	public void validate_the_status_code_after_missing_the_password() {
		response.then().spec(testContext.utils.validateCodeForIncorrectDetails());
	  
	}

	@Then("Validate the response body after missing the password")
	public void validate_the_response_body_after_missing_the_password() {
		JsonPath jsonPath = response.then().extract().jsonPath();
		String message = jsonPath.getString(API_Constants.MESSAGE);
		String code = jsonPath.getString(API_Constants.BODY_CODE);
		Assert.assertEquals(code, API_Constants.CODE_FOR_INVALID_FORMAT);
		Assert.assertEquals(message, API_Constants.MESSAGE_FOR_MISSING_FIELDS);
	}

	@When("User makes a post request with the password having less characters {string} and {string}")
	public void user_makes_a_post_request_with_the_password_having_less_characters_and(String username, String password) {
		response = requestSpecificationForUser.contentType(ContentType.JSON).
				body(testContext.userData.createUserWithPasswordLessThanRequiredCharacters(username, password)).when().post(API_Constants.CREATE_USER_PATH);
	}

	@Then("Validate the status code after entering password with less characters")
	public void validate_the_status_code_after_entering_password_with_less_characters() {
		response.then().spec(testContext.utils.validateCodeForIncorrectDetails());
	}

	@Then("Validate the response body after entering password with less characters")
	public void validate_the_response_body_after_entering_password_with_less_characters() {
		JsonPath jsonPath = response.then().extract().jsonPath();
		String message = jsonPath.getString(API_Constants.MESSAGE);
		String code = jsonPath.getString(API_Constants.BODY_CODE);
		Assert.assertEquals(code, API_Constants.CODE_FOR_INCORRECT_PASSWORD);
		Assert.assertEquals(message, API_Constants.MESSAGE_FOR_INVALID_PASSWORD_FORMAT);
	}


}
