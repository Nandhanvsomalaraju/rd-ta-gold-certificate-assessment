package com.miniproject.api.StepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.*;

import java.io.IOException;

import com.miniproject.api.pojos.BookInfoPOJO;
import com.miniproject.api.pojos.BookPojo;
import com.miniproject.api.testdata.APITestContextSetup;
import com.miniproject.constants.API_Constants;

public class BookInfoStepDefinition {
	
	RequestSpecification requestSpecification;
	Response response;
	BookPojo book;
	public static JsonPath jsonPathOfBooks;
	APITestContextSetup testContext;
	public BookInfoStepDefinition(APITestContextSetup testContext)
	{
		this.testContext = testContext;
	}
	
	@Given("User Hits The Base URI")
	public void user_hits_the_base_uri() throws IOException {
		requestSpecification = given().spec(testContext.utils.requestforBook());
	}

	@When("User Sends A Get Request")
	public void user_sends_a_get_request() {
		response = requestSpecification.when().get(API_Constants.GET_BOOKS_PATH);
	}

	@Then("Save The Response Obtained")
	public void save_the_response_obtained() {
		  jsonPathOfBooks = response.then().extract().response().jsonPath();
		  jsonPathOfBooks.getString("books[0]");

	}

}
